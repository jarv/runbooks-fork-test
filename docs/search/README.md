<!-- MARKER: do not edit this section directly. Edit services/service-catalog.yml then run scripts/generate-docs -->

**Table of Contents**

[[_TOC_]]

#  Search Service
* **Alerts**: https://alerts.gitlab.net/#/alerts?filter=%7Btype%3D%22search%22%2C%20tier%3D%22inf%22%7D
* **Label**: gitlab-com/gl-infra/production~"Service:Elasticsearch"

## Logging

* [elastic stack monitoring](https://00a4ef3362214c44a044feaa539b4686.us-central1.gcp.cloud.es.io:9243/app/monitoring#/overview?_g=(cluster_uuid:D31oWYIkTUWCDPHigrPwHg))

## Troubleshooting Pointers

* [../ci-runners/ci-runner-timeouts.md](../ci-runners/ci-runner-timeouts.md)
* [../ci-runners/service-ci-runners.md](../ci-runners/service-ci-runners.md)
* [../cloudflare/logging.md](../cloudflare/logging.md)
* [../cloudflare/troubleshooting.md](../cloudflare/troubleshooting.md)
* [../config_management/chef-guidelines.md](../config_management/chef-guidelines.md)
* [../config_management/chef-troubleshooting.md](../config_management/chef-troubleshooting.md)
* [../config_management/chef-workflow.md](../config_management/chef-workflow.md)
* [../elastic/elasticsearch-integration-in-gitlab.md](../elastic/elasticsearch-integration-in-gitlab.md)
* [../gitaly/find-project-from-hashed-storage.md](../gitaly/find-project-from-hashed-storage.md)
* [../gitaly/gitaly-permission-denied.md](../gitaly/gitaly-permission-denied.md)
* [../gitaly/gitaly-unusual-activity.md](../gitaly/gitaly-unusual-activity.md)
* [../pages/gitlab-pages.md](../pages/gitlab-pages.md)
* [../patroni/performance-degradation-troubleshooting.md](../patroni/performance-degradation-troubleshooting.md)
* [../patroni/pg_collect_query_data.md](../patroni/pg_collect_query_data.md)
* [../patroni/postgresql-role-credential-rotation.md](../patroni/postgresql-role-credential-rotation.md)
* [../patroni/rails-sql-apdex-slow.md](../patroni/rails-sql-apdex-slow.md)
* [../praefect/praefect-error-rate.md](../praefect/praefect-error-rate.md)
* [../redis/redis-survival-guide-for-sres.md](../redis/redis-survival-guide-for-sres.md)
* [../sidekiq/sidekiq-survival-guide-for-sres.md](../sidekiq/sidekiq-survival-guide-for-sres.md)
* [../sidekiq/sidekiq_error_rate_high.md](../sidekiq/sidekiq_error_rate_high.md)
* [../uncategorized/camoproxy.md](../uncategorized/camoproxy.md)
* [../uncategorized/domain-registration.md](../uncategorized/domain-registration.md)
* [../uncategorized/gcp-network-intelligence.md](../uncategorized/gcp-network-intelligence.md)
* [../uncategorized/gcp-project.md](../uncategorized/gcp-project.md)
* [../uncategorized/gcp-snapshots.md](../uncategorized/gcp-snapshots.md)
* [../uncategorized/k8s-operations.md](../uncategorized/k8s-operations.md)
* [../uncategorized/kubernetes.md](../uncategorized/kubernetes.md)
* [../uncategorized/manage-chef.md](../uncategorized/manage-chef.md)
* [../uncategorized/node-reboots.md](../uncategorized/node-reboots.md)
* [../uncategorized/uploads.md](../uncategorized/uploads.md)
<!-- END_MARKER -->

<!-- ## Summary -->

<!-- ## Architecture -->

<!-- ## Performance -->

<!-- ## Scalability -->

<!-- ## Availability -->

<!-- ## Durability -->

<!-- ## Security/Compliance -->

<!-- ## Monitoring/Alerting -->

<!-- ## Links to further Documentation -->
